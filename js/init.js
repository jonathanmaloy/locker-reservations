//initate app
resApp.init();

// Check a bag in
document.getElementById('useLocker').addEventListener('submit', function(e) {
	e.preventDefault();
	var selected = this.elements.size.selectedIndex;
    var lockerNumber = resApp.useLocker(this.elements.size.options[selected].value);
	if(lockerNumber !== -1) {
		document.getElementById('ticket-number').innerHTML = lockerNumber;	
		swal({
			title: 'Locker ' + lockerNumber,
			text: 'Please place the customer\'s bag in locker ' + lockerNumber,
			type: 'success',
			allowOutsideClick: false,
			confirmButtonText: 'Print Claim Ticket'
		}).then(function() {
			window.print();
		});
	} else {
		document.getElementById('ticket-number').innerHTML = '';
		swal({
			title: 'Uh Oh!',
			text: 'Sorry, there are no lockers that will fit the bag.',
			type: 'warning',
			allowOutsideClick: false,
			confirmButtonText: 'I Understand'
		});
	}
	this.reset();
}, false);

// Return bag to customer
document.getElementById('getBag').addEventListener('submit', function(e) {
	e.preventDefault();
    var lockerNumber = resApp.emptyLocker(this.elements.locker.value);
	if(lockerNumber !== -1) {
		swal({
			title: 'Locker ' + lockerNumber,
			text: 'Please retrieve the customer\'s bag from locker ' + lockerNumber,
			type: 'success',
			allowOutsideClick: false,
			confirmButtonText: 'Done'
		});
	} else {
		document.getElementById('ticket-number').innerHTML = '';
		swal({
			title: 'Uh Oh!',
			text: 'Sorry, this locker is not in use.',
			type: 'warning',
			allowOutsideClick: false,
			confirmButtonText: 'I Understand'
		});
	}
	this.reset();
}, false);