var resApp = (function () {
	'use strict';
	var smallLockers = [];
	var mediumLockers = [];
	var largeLockers = [];
	var lockersInUse = [];
	
	function resetLockers() {
		lockersInUse = [];
		for(var s = 1; s <= 1000; s++) {
			smallLockers.push(s);
		}
		for(var m = 1001; m <= 2000; m++) {
			mediumLockers.push(m);
		}
		for(var l = 2001; l <= 3000; l++) {
			largeLockers.push(l);
		}
	}
	
	function useLocker(size) {
		var lockerNumber;
		switch(size) {
			case 'small':
			lockerNumber = smallLockers.shift();
			break;
			case 'medium':
			lockerNumber = mediumLockers.shift();
			break;
			case 'large':
			lockerNumber = largeLockers.shift();
			break;
		}
		lockersInUse.push({'locker':lockerNumber,'size':size});		
		return lockerNumber;
	}
	
	function lockerRequest(size) {
		var lockerNumber;
		switch(size) {
			case 'small':
				if(smallLockers.length > 0) {
					lockerNumber = useLocker('small');
				} else if(mediumLockers.length > 0) {
					lockerNumber = useLocker('medium');
				} else if(largeLockers.length > 0) {
					lockerNumber = useLocker('large');
				} else {
					return -1;
				}
				return lockerNumber;
			case 'medium':
				if(mediumLockers.length > 0) {
					lockerNumber = useLocker('medium');
				} else if(largeLockers.length > 0) {
					lockerNumber = useLocker('large');
				} else {
					return -1;
				}
				return lockerNumber;
			case 'large':
				if(largeLockers.length > 0) {
					lockerNumber = useLocker('large');
				} else {
					return -1;
				}
				return lockerNumber;
			default:
				return -1;			
		}
	}
	
	function returnLockerContents(lockerNumber) {
		var inUseObj = lockersInUse.filter(function(o){return o.locker === parseInt(lockerNumber);} );
		inUseObj = inUseObj ? inUseObj[0] : false;
		if(!(inUseObj)) {
			return -1;
		} else {
			switch(inUseObj.size) {
				case 'small':
					smallLockers.push(parseInt(lockerNumber));
					break;
				case 'medium':
					mediumLockers.push(parseInt(lockerNumber));
					break;
				case 'large':
					largeLockers.push(parseInt(lockerNumber));
					break;		
			}
			lockersInUse = lockersInUse.filter(function( obj ) {
				return obj.locker !== parseInt(lockerNumber);
			});
			return lockerNumber;
		}
	}	
	
	return {
		init: function () {
			resetLockers();
		},
		useLocker: function(size) {
			return lockerRequest(size);
		},
		emptyLocker: function(lockerNumber) {
			return returnLockerContents(lockerNumber);
		}
	};
})();
